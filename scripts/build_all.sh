#!/usr/bin/bash
#

git_clone_or_sync() {

  local directory
  local repository
  local tag

  directory="$1"
  repository="$2"
  tag="$3"

  [ -d "$directory" ] || /usr/bin/git clone "$repository"
  cd "$directory"
  branch="$(/usr/bin/git tags|grep "$tag"|grep -v -- -|tail -1)"

  if /usr/bin/git branch | grep -q "$branch"; then
	  # Branch exists and checkout if i'ts note the current branch
	  [ "$(/usr/bin/git rev-parse --abbrev-ref HEAD)" == "$branch" ] \
		  || /usr/bin/git checkout "$branch"
  else
	  # Branch doesn't exist.
	  /usr/bin/git checkout -b "$branch" "$branch"
  fi

  printf -- "%s" "$branch"
}



TMPDIR="${TMPDIR:-/tmp}"
KUBE_VERSION="${KUBE_VERSION:-v1.28}"
ETCD_VERSION="${ETCD_VERSION:-v3.4}"
CONTAINERD_VERSION="${CONTAINERD_VERSION:-v1.7}"
CNI_VERSION="${CNI_VERSION:-v1.3}"
BUILDDIR="${BUILDDIR:-"$(realpath .)"/src}"
ARTEFACTSDIR="${ARTEFACTSDIR:-"$(realpath .)"/data}"

[ -f .env ] && . .env

set -euo pipefail


[ -d "$BUILDDIR" ] || mkdir -p "$BUILDDIR"
[ -d "$ARTEFACTSDIR" ] || mkdir -p "$ARTEFACTSDIR"

# Building kubernetes

cd  "$BUILDDIR"
branch="$(git_clone_or_sync kubernetes https://github.com/kubernetes/kubernetes "${KUBE_VERSION}")"
cd kubernetes/

for c in kube-apiserver kube-controller-manager kube-scheduler kubectl kubelet kube-proxy kubeadm
do
  (cd cmd/"$c" && TMPDIR="$TMPDIR" GO_BUILD_FLAGS="-v" go build && cp "$c" "$ARTEFACTSDIR"/"$c")
done

# Building etcd

cd  "$BUILDDIR"
branch="$(git_clone_or_sync  etcd https://github.com/etcd-io/etcd "${ETCD_VERSION}")"
cd etcd/
go mod vendor
TMPDIR="${TMPDIR}" GO_BUILD_FLAGS="-v" ./build

[ -d etcd-"$branch"-linux-amd64 ]  || mkdir etcd-"$branch"-linux-amd64
cp bin/etcd{,ctl}  etcd-"$branch"-linux-amd64
tar czvf "$ARTEFACTSDIR"/etcd-"$branch"-linux-amd64.tar.gz etcd-"$branch"-linux-amd64


# Building containerd

cd  "$BUILDDIR"
branch="$(git_clone_or_sync containerd https://github.com/containerd/containerd "${CONTAINERD_VERSION}")"
cd containerd/
TMPDIR="${TMPDIR}" GO_BUILD_FLAGS="-v" make
tar czvf "$ARTEFACTSDIR"/containerd-"$branch"-amd64.tar.gz bin/containerd* bin/ctr


# Building cri-tools

cd  "$BUILDDIR"
branch="$(git_clone_or_sync cri-tools https://github.com/kubernetes-sigs/cri-tools "${KUBE_VERSION}")"

cd cri-tools/
TMPDIR="$TMPDIR" GO_BUILD_FLAGS="-v" make binaries
( cd build/bin/linux/amd64/ && tar czvf "$ARTEFACTSDIR"/crictl-"$branch"-linux-amd64.tar.gz crictl )

# Building runc

cd  "$BUILDDIR"
branch="$(git_clone_or_sync runc https://github.com/opencontainers/runc main)"
cd runc/
TMPDIR="$TMPDIR" GO_BUILD_FLAGS="-v" make runc
cp runc "$ARTEFACTSDIR"/runc.amd64

# Building cni-plugins-linux

cd  "$BUILDDIR"
branch="$(git_clone_or_sync plugins https://github.com/containernetworking/plugins "${CNI_VERSION}")"
cd plugins/

TMPDIR="$TMPDIR" GO_BUILD_FLAGS="-v" ./build_linux.sh
( cd bin && tar czvf "$ARTEFACTSDIR"/cni-plugins-linux-amd64-"$branch".tgz * )
cd ..

cd "$ARTEFACTSDIR" && pwd && ls -loh *

