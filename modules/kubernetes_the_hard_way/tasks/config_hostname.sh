#!/usr/bin/bash
#
#
set -eu


HOST="${PT_hostname}"
FQDN="${PT_fqdn}"

sed -i "s/^127.0.1.1.*/127.0.1.1\t${FQDN} ${HOST}/" /etc/hosts
hostnamectl hostname "${HOST}"
