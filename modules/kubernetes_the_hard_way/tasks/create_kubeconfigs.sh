#!/usr/bin/bash

set -ue

caconf="${PT_caconf:-certs/ca.conf.local}"
certsdir="${PT_certsdir:-certs}"
kubebindir="${PT_kubebindir:-data}"
outputdir="${PT_outputdir:-data}"

[ -d "$outputdir" ] || mkdir -p "$outputdir"


# Nodes kube config

for host in node-0 node-1; do
  "$kubebindir"/kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority="$caconf" \
    --embed-certs=true \
    --server=https://server.kubernetes.local:6443 \
    --kubeconfig="$outputdir"/${host}.kubeconfig

  "$kubebindir"/kubectl config set-credentials system:node:${host} \
    --client-certificate="$certsdir"/${host}.crt \
    --client-key="$certsdir"/${host}.key \
    --embed-certs=true \
    --kubeconfig="$outputdir"/${host}.kubeconfig

  "$kubebindir"/kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:node:${host} \
    --kubeconfig="$outputdir"/${host}.kubeconfig

  "$kubebindir"/kubectl config use-context default \
    --kubeconfig="$outputdir"/${host}.kubeconfig
done

# kube-proxy

{
  "$kubebindir"/kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority="$caconf" \
    --embed-certs=true \
    --server=https://server.kubernetes.local:6443 \
    --kubeconfig="$outputdir"/kube-proxy.kubeconfig

  "$kubebindir"/kubectl config set-credentials system:kube-proxy \
    --client-certificate="$certsdir"/kube-proxy.crt \
    --client-key="$certsdir"/kube-proxy.key \
    --embed-certs=true \
    --kubeconfig="$outputdir"/kube-proxy.kubeconfig

  "$kubebindir"/kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-proxy \
    --kubeconfig="$outputdir"/kube-proxy.kubeconfig

  "$kubebindir"/kubectl config use-context default \
    --kubeconfig="$outputdir"/kube-proxy.kubeconfig
}


# kube-controler
{
  "${kubebindir}"/kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority="$certsdir"/ca.crt \
    --embed-certs=true \
    --server=https://server.kubernetes.local:6443 \
    --kubeconfig="$outputdir"/kube-controller-manager.kubeconfig

  "${kubebindir}"/kubectl config set-credentials system:kube-controller-manager \
    --client-certificate="$certsdir"/kube-controller-manager.crt \
    --client-key="$certsdir"/kube-controller-manager.key \
    --embed-certs=true \
    --kubeconfig="$outputdir"/kube-controller-manager.kubeconfig

  "${kubebindir}"/kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-controller-manager \
    --kubeconfig="$outputdir"/kube-controller-manager.kubeconfig

  "${kubebindir}"/kubectl config use-context default \
    --kubeconfig="$outputdir"/kube-controller-manager.kubeconfig
}

# Kube scheduler

{
  "$kubebindir"/kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority="$caconf" \
    --embed-certs=true \
    --server=https://server.kubernetes.local:6443 \
    --kubeconfig="$outputdir"/kube-scheduler.kubeconfig

  "$kubebindir"/kubectl config set-credentials system:kube-scheduler \
    --client-certificate="$certsdir"/kube-scheduler.crt \
    --client-key="$certsdir"/kube-scheduler.key \
    --embed-certs=true \
    --kubeconfig="$outputdir"/kube-scheduler.kubeconfig

  "$kubebindir"/kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-scheduler \
    --kubeconfig="$outputdir"/kube-scheduler.kubeconfig

  "$kubebindir"/kubectl config use-context default \
    --kubeconfig="$outputdir"/kube-scheduler.kubeconfig
}

# kube Admin

{
  "$kubebindir"/kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority="$caconf" \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig="$outputdir"/admin.kubeconfig

  "$kubebindir"/kubectl config set-credentials admin \
    --client-certificate="$certsdir"/admin.crt \
    --client-key="$certsdir"/admin.key \
    --embed-certs=true \
    --kubeconfig="$outputdir"/admin.kubeconfig

  "$kubebindir"/kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=admin \
    --kubeconfig="$outputdir"/admin.kubeconfig

  "$kubebindir"/kubectl config use-context default \
    --kubeconfig="$outputdir"/admin.kubeconfig
}

