#!/usr/bin/bash
#

set -e

caconf="${PT_caconf:-"$(realpath .)"/ca.conf}"
certsdir="${PT_certsdir:-"$(realpath .)"/certs}"
country="${PT_country:-VU}"
state="${PT_state:-Efate}"
locality="${PT_locality:-Port-Vila}"

[ -f .env ] && . .env

set -u

[ -f "$caconf" ] || {
	printf -- "[-] error ca conf file %s does not exists\n" "$caconf"
    exit 1
}

set -x

[ -d "$certsdir" ] || mkdir -p "$certsdir"
cd "$certsdir"

localcaconf="$certsdir"/ca.conf.local
sed -e "s/= US/= $country/" \
    -e "s/= Washington/= $state/" \
	-e "s/= Seattle/= $locality/" "$caconf" > "$localcaconf"


openssl genrsa -out ca.key 4096
openssl req -x509 -new -sha512 -noenc \
  -key ca.key -days 3653 \
  -config "$localcaconf" \
  -out ca.crt


certs=(
  "admin" "node-0" "node-1"
  "kube-proxy" "kube-scheduler"
  "kube-controller-manager"
  "kube-api-server"
  "service-accounts"
)

for i in ${certs[*]}; do
  openssl genrsa -out "${i}.key" 4096

  openssl req -new -key "${i}.key" -sha256 \
    -config "$localcaconf" -section ${i} \
    -out "${i}.csr"

  openssl x509 -req -days 3653 -in "${i}.csr" \
    -copy_extensions copyall \
    -sha256 -CA "ca.crt" \
    -CAkey "ca.key" \
    -CAcreateserial \
    -out "${i}.crt"
done

ls -1 *.crt *.key *.csr
