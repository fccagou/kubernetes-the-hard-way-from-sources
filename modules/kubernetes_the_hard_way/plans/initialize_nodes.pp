plan kubernetes_the_hard_way::initialize_nodes (
  TargetSpec $targets,
) {
    get_targets($targets).each |$t| {
      # Configure hostname
      run_task('kubernetes_the_hard_way::config_hostname', $t,
                               'hostname' => $t.name,
                               'fqdn' => $t.uri
      )
    }
}
