# Build binaries and configure nodes
# WARNING: This is an autogenerated plan. It might not behave as expected.
# @param caconf
# @param certsdir
# @param country
# @param state
# @param locality
plan kubernetes_the_hard_way(
  String[1] $caconf,
  String[1] $certsdir,
  String[1] $kubebindir,
  String[1] $kubeconfig_outputdir,
  String[1] $state = 'Efate',
  String[1] $country = 'VU',
  String[1] $locality = 'Port-Vila',
) {
  # Configure hostname and /etc/hosts
  run_plan('kubernetes_the_hard_way::initialize_nodes', 'all')
  # Generate ca ans certificats for all serivices
  run_task('kubernetes_the_hard_way::certs_init', 'localhost', {'caconf' => $caconf, 'certsdir' => $certsdir, 'country' => $country, 'state' => $state, 'locality' => $locality})
  # Generate kubeconfig files in data
  run_task('kubernetes_the_hard_way::create_kubeconfigs', 'localhost', {'caconf' => "$certsdir/ca.conf.local", 'certsdir' => $certsdir, 'kubebindir' => $kubebindir, 'outputdir' => $kubeconfig_outputdir })
  # Configure control-plane
  run_plan('kubernetes_the_hard_way::configure_server', {'server' => 'server', 'certsdir' => $certsdir, 'kubeconfig_dir' => $kubeconfig_outputdir})
  # Configure kubelets on nodes
  run_plan('kubernetes_the_hard_way::configure_kubelet', {'nodes' => 'nodes', 'certsdir' => $certsdir, 'kubeconfig_dir' => $kubeconfig_outputdir})
  run_plan('kubernetes_the_hard_way::configure_kube_proxy', {'nodes' => 'nodes', 'kubeconfig_dir' => $kubeconfig_outputdir})
}

