#
#
#

plan kubernetes_the_hard_way::configure_kube_proxy(
  TargetSpec $nodes,
  String $kubeconfig_dir,
) {

  run_command('mkdir -p /var/lib/kube-proxy && chmod 700 /var/lib/kube-proxy', $nodes)
  upload_file("${kubeconfig_dir}/kube-proxy.kubeconfig", "/var/lib/kube-proxy/kubeconfig", $nodes)

}
