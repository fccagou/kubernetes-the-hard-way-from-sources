#
#
#

plan kubernetes_the_hard_way::configure_server(
  TargetSpec $server,
  String $certsdir,
  String $kubeconfig_dir,
) {

  [
    'ca.key',
    'ca.crt',
    'kube-api-server.key',
    'kube-api-server.crt',
    'service-accounts.key',
    'service-accounts.crt',
  ].each |$f| {
    upload_file("${certsdir}/${f}", "/root/${f}", $server)
  }

  [
    'admin.kubeconfig',
    'kube-controller-manager.kubeconfig',
    'kube-scheduler.kubeconfig',
  ].each |$f| {
    upload_file("${kubeconfig_dir}/${f}", "/root/${f}", $server)
  }
}
