#
#
#

plan kubernetes_the_hard_way::configure_kubelet(
  TargetSpec $nodes,
  String $certsdir,
  String $kubeconfig_dir,
) {

  run_command('mkdir -p /var/lib/kubelet && chmod 700 /var/lib/kubelet', $nodes)
  upload_file("${certsdir}/ca.crt", '/var/lib/kubelet/ca.crt', $nodes)

  get_targets($nodes).each |$n| {
    upload_file("${certsdir}/${n.name}.key", "/var/lib/kubelet/${n.name}.key", $n)
    upload_file("${certsdir}/${n.name}.crt", "/var/lib/kubelet/${n.name}.crt", $n)
    upload_file("${kubeconfig_dir}/${n.name}.kubeconfig", "/var/lib/kubelet/kubeconfig", $n)
  }
}
