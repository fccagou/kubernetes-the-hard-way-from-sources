# Build "kubernetes the hard way" binaries from sources and Bolt deployment

## Presentation

In [kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/02-jumpbox.md#download-binaries) all binaries are downloaded.

In this project, we build the packages for to get all the sources in registry
and use [puppet bolt](https://www.puppet.com/docs/bolt/latest) to deploy binaries
on nodes.


## Quick run

Running `./scripts/build_all.sh` creates files under `./data` dir.

Env variables can be set to changes versions, tmpdir or artefacts dir.
The script also source `.env` file if exists.

```bash
TMPDIR="${TMPDIR:-/tmp}"
KUBE_VERSION="${KUBE_VERSION:-v1.28}"
ETCD_VERSION="${ETCD_VERSION:-v3.4}"
CONTAINERD_VERSION="${CONTAINERD_VERSION:-v1.7}"
CNI_VERSION="${CNI_VERSION:-v1.3}"
BUILDDIR="${BUILDDIR:-"$(realpath .)"/src}"
ARTEFACTSDIR="${ARTEFACTSDIR:-"$(realpath .)"/data}"
```

## Using puppet bolt

Using puppet bolt, just run

```bash
bolt plan run kubernetes_the_hard_way \
    caconf="$(pwd)"/ca.conf \
    certsdir="$(pwd)"/certs \
    kubebindir="$(pwd)"/data \
    kubeconfig_outputdir="$(pwd)"/data \
```


# TODO

- [ ] pass git url as parameters for airgap
- [ ] sign artefacts
- [ ] make SBOM and sign it
- [ ] keep all sources code in vault




